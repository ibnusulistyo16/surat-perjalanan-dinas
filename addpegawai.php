<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
<link rel="stylesheet" type="text/css" href="plugins/tigra_calendar/tcal.css"/>
<script type="text/javascript" src="plugins/tigra_calendar/tcal.js"></script>  
</head>
<body>   
<h1>TAMBAH DATA PEGAWAI </h1>
     
<?php
error_reporting(0);
if (!defined('BASEPATH')) exit('No direct script access allowed');


# SKRIP SAAT TOMBOL SIMPAN DIKLIK
if(isset($_POST['btnSimpan'])){
	# Baca Variabel Data Form
	$nip		= mysql_real_escape_string($_POST['nip']);
	$nama_pegawai	= mysql_real_escape_string($_POST['nama_pegawai']);
	$pangkat	= mysql_real_escape_string($_POST['pangkat']);
	$golongan	= mysql_real_escape_string($_POST['golongan']);
	$jabatan	= mysql_real_escape_string($_POST['jabatan']);
	$tmp_p	= mysql_real_escape_string($_POST['tmp_p']);
	$tgl_p	= mysql_real_escape_string($_POST['tgl_p']);
	$alamat		= mysql_real_escape_string($_POST['alamat']);
  $jk   = mysql_real_escape_string($_POST['jk']);
	
	# Validasi form, jika kosong sampaikan pesan error
	$pesanError = array();
	if (trim($nip)=="") {
		$pesanError[] = "Data <b>NIP pegawai</b> tidak boleh kosong !";		
	}
	if (trim($nama_pegawai)=="") {
		$pesanError[] = "Data <b>Nama pegawai</b> tidak boleh kosong!";		
	}
	if (trim($pangkat)=="") {
		$pesanError[] = "Data <b>Tanggal Lahir pegawai</b> tidak boleh kosong!";		
	}
	if (trim($golongan)=="") {
		$pesanError[] = "Data <b>Tanggal Lahir pegawai</b> tidak boleh kosong!";		
	}
	if (trim($jabatan)=="") {
		$pesanError[] = "Data <b>Tanggal Lahir pegawai</b> tidak boleh kosong!";		
	}
	if (trim($tmp_p)=="") {
		$pesanError[] = "Data <b>Tempat Lahir pegawai</b> tidak boleh kosong!";		
	}
	if (trim($tgl_p)=="") {
		$pesanError[] = "Data <b>Tanggal Lahir pegawai</b> tidak boleh kosong!";		
	}
	if (trim($jk)=="KOSONG") {
		$pesanError[] = "Data <b>Jenis Kelamin pegawai</b> belum ada yang dipilih !";		
	}
	if (trim($alamat)=="") {
		$pesanError[] = "Data <b>Alamat pegawai</b> tidak boleh kosong!";		
	}
	
	$cekdata="select nip from pegawai where nip='$nip'";
	$ada=mysql_query($cekdata) or die(mysql_error());
	if(mysql_num_rows($ada)>0){
				$pesanError[] = "Data <b>No Induk pegawai Sudah Ada !</b> belum ada yang dipilih !";		
	}

	# JIKA ADA PESAN ERROR DARI VALIDASI
	if (count($pesanError)>=1 ){
		echo "<div class='mssgBox'>";
		$noPesan=0;
		foreach ($pesanError as $indeks=>$pesan_tampil) { 
			$noPesan++;
			echo "&nbsp;&nbsp; $noPesan.$pesan_tampil<br>";	
			}
		echo "</div> <br>"; 
	}
	else {
		# SIMPAN DATA KE DATABASE. // Jika tidak menemukan error, simpan data ke database
		
		$mySql	= "INSERT INTO pegawai (nip, nama_pegawai,pangkat,golongan,jabatan, tmp_p, tgl_p,jk, alamat) 
						VALUES ('$nip',
								'$nama_pegawai',
								'$pangkat',
								'$golongan',
								'$jabatan',
								'$tmp_p',
								'$tgl_p',
								'$jk',
								'$alamat')";
							
		$myQry	= mysql_query($mySql, $koneksidb) or die ("Gagal query".mysql_error());
		if($myQry){
			echo "<script>alert('Data pegawai Berhasil di simpan')</script>";
			echo "<meta http-equiv='refresh' content='0; url=?page=data_pegawai'>";
		}
		exit;
	}
} // Penutup POST
	
	
# VARIABEL DATA UNTUK DIBACA FORM

$dataTanggal 	= isset($_POST['tgl_p']) ? $_POST['tgl_p'] : date('d-m-Y');
?>
<!-- Modal Dialog Contact -->
<form action="<?php $_SERVER['PHP_SELF']; ?>" method="post"    >
  <table width="908" align="left" class="table table-striped table-bordered table-condensed">
    
    
    <tr>
      <td colspan="3"><strong>INPUT DATA pegawai*</strong></td>
    </tr>
    <tr>
      <td width="21%"><strong>NIP </strong></td>
      <td width="2%"><strong>:</strong></td>
      <td width="77%"><b>
        <input name="nip" type="text"  size="20" maxlength="25"  />
      </b></td>
    </tr>
    <tr>
      <td><strong>Nama pegawai</strong></td>
      <td><b>:</b></td>
      <td><b>
        <input name="nama_pegawai" type="text" size="25" maxlength="40"  />
      </b></td>
    </tr>
    <tr>
      <td><strong>Pangkat, Golongan</strong></td>
      <td><strong>:</strong></td>
      <td><input name="pangkat"  size="10" type="text" maxlength="20" />
        /
          <input name="golongan" type="text" size="3" maxlength="3" /></td>
    </tr>
    <tr>
      <td><strong>Jabatan</strong></td>
      <td><b>:</b></td>
      <td><b>
        <input name="jabatan" type="text" size="70" maxlength="100"  />
      </b></td>
    </tr>
    <tr>
      <td><strong>Tempat, Tanggal Lahir</strong></td>
      <td><strong>:</strong></td>
      <td><input name="tmp_p"  size="23" type="text" maxlength="20" />
        .
          <input name="tgl_p" type="text" class="tcal" value="<?php echo $dataTanggal; ?>" size="20" maxlength="20" /></td>
    </tr>
	<tr>
	  <td><strong>Jenis Kelamin</strong></td>
	  <td><b>:</b></td>
	  <td>
            
            <select name="jk"  >
             <option value="KOSONG">Pilih :</option>
              <option value="Laki-Laki">Laki-Laki</option>
              <option value="Perempuan">Perempuan</option>
            </select>
      </td>
    </tr>
	<tr>
      <td><strong>Alamat</strong></td>
      <td><strong>:</strong></td>
      <td><textarea name="alamat"   cols="50"></textarea></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td><b>
      <button class="btn btn-primary" name="btnSimpan" type="submit"   ><i class="fa fa-save"></i> Simpan</button>
      </b></td>
    </tr>
   
  </table>
  
<br>
</form>
</body>
</html>