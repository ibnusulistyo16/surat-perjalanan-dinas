<?php
require('fpdf/fpdf.php');
include_once "koneksi.php";
if(isset($_GET['no_surat'])){
	// Membaca nomor penjualan dari URL
	$noNota = $_GET['no_surat'];
	$awal= "SELECT * from tmp_surat WHERE no_surat='$noNota'";
	$myQry = mysql_query($awal, $koneksidb)  or die ("Query salah : ".mysql_error());
	//$kolomData = mysql_fetch_array($myQry);
}
else {
	echo "Nomor Surat (no_surat) tidak ditemukan";
	exit;
}
// =========================================================================================================================

function hex2dec($couleur = "#000000"){
    $R = substr($couleur, 1, 2);
    $rouge = hexdec($R);
    $V = substr($couleur, 3, 2);
    $vert = hexdec($V);
    $B = substr($couleur, 5, 2);
    $bleu = hexdec($B);
    $tbl_couleur = array();
    $tbl_couleur['R']=$rouge;
    $tbl_couleur['G']=$vert;
    $tbl_couleur['B']=$bleu;
    return $tbl_couleur;
}

//conversion pixel -> millimeter in 72 dpi
function px2mm($px){
    return $px*25.4/72;
}

function txtentities($html){
    $trans = get_html_translation_table(HTML_ENTITIES);
    $trans = array_flip($trans);
    return strtr($html, $trans);
}

#================================================================================================================
class pdf extends FPDF{
protected $B;
protected $I;
protected $U;
protected $HREF;
protected $fontList;
protected $issetfont;
protected $issetcolor;

function __construct($orientation='P', $unit='mm', $format='legal')
{
    // $format='A4'
    //Call parent constructor
    parent::__construct($orientation, $unit, $format);

    //Initialization
    $this->B=0;
    $this->I=0;
    $this->U=0;
    $this->HREF='';

    $this->tableborder=0;
    $this->tdbegin=false;
    $this->tdwidth=0;
    $this->tdheight=0;
    $this->tdalign="L";
    $this->tdbgcolor=false;

    $this->oldx=0;
    $this->oldy=0;

    $this->fontlist=array("arial", "times", "courier", "helvetica", "symbol");
    $this->issetfont=false;
    $this->issetcolor=false;
}

//////////////////////////////////////
//html parser

function WriteHTML($html)
{
    $html=strip_tags($html, "<b><u><i><a><img><p><br><strong><em><font><tr><blockquote><hr><td><tr><table><sup>"); //remove all unsupported tags
    $html=str_replace("\n", '', $html); //replace carriage returns with spaces
    $html=str_replace("\t", '', $html); //replace carriage returns with spaces
    $a=preg_split('/<(.*)>/U', $html, -1, PREG_SPLIT_DELIM_CAPTURE); //explode the string
    foreach($a as $i=>$e)
    {
        if($i%2==0)
        {
            //Text
            if($this->HREF)
                $this->PutLink($this->HREF, $e);
            elseif($this->tdbegin) {
                if(trim($e)!='' && $e!="&nbsp;") {
                    $this->Cell($this->tdwidth, $this->tdheight, $e, $this->tableborder, '', $this->tdalign, $this->tdbgcolor);
                }
                elseif($e=="&nbsp;") {
                    $this->Cell($this->tdwidth, $this->tdheight, '', $this->tableborder, '', $this->tdalign, $this->tdbgcolor);
                }
            }
            else
                $this->Write(5, stripslashes(txtentities($e)));
        }
        else
        {
            //Tag
            if($e[0]=='/')
                $this->CloseTag(strtoupper(substr($e, 1)));
            else
            {
                //Extract attributes
                $a2=explode(' ', $e);
                $tag=strtoupper(array_shift($a2));
                $attr=array();
                foreach($a2 as $v)
                {
                    if(preg_match('/([^=]*)=["\']?([^"\']*)/', $v, $a3))
                        $attr[strtoupper($a3[1])]=$a3[2];
                }
                $this->OpenTag($tag, $attr);
            }
        }
    }
}

function OpenTag($tag, $attr)
{
    //Opening tag
    switch($tag){

        case 'SUP':
            if( !empty($attr['SUP']) ) {    
                //Set current font to 6pt     
                $this->SetFont('', '', 6);
                //Start 125cm plus width of cell to the right of left margin         
                //Superscript "1" 
                $this->Cell(2, 2, $attr['SUP'], 0, 0, 'L');
            }
            break;

        case 'TABLE': // TABLE-BEGIN
            if( !empty($attr['BORDER']) ) $this->tableborder=$attr['BORDER'];
            else $this->tableborder=0;
            break;
        case 'TR': //TR-BEGIN
            break;
        case 'TD': // TD-BEGIN
            if( !empty($attr['WIDTH']) ) $this->tdwidth=($attr['WIDTH']/4);
            else $this->tdwidth=40; // Set to your own width if you need bigger fixed cells
            if( !empty($attr['HEIGHT']) ) $this->tdheight=($attr['HEIGHT']/6);
            else $this->tdheight=6; // Set to your own height if you need bigger fixed cells
            if( !empty($attr['ALIGN']) ) {
                $align=$attr['ALIGN'];        
                if($align=='LEFT') $this->tdalign='L';
                if($align=='CENTER') $this->tdalign='C';
                if($align=='RIGHT') $this->tdalign='R';
            }
            else $this->tdalign='L'; // Set to your own
            if( !empty($attr['BGCOLOR']) ) {
                $coul=hex2dec($attr['BGCOLOR']);
                    $this->SetFillColor($coul['R'], $coul['G'], $coul['B']);
                    $this->tdbgcolor=true;
                }
            $this->tdbegin=true;
            break;

        case 'HR':
            if( !empty($attr['WIDTH']) )
                $Width = $attr['WIDTH'];
            else
                $Width = $this->w - $this->lMargin-$this->rMargin;
            $x = $this->GetX();
            $y = $this->GetY();
            $this->SetLineWidth(0.2);
            $this->Line($x, $y, $x+$Width, $y);
            $this->SetLineWidth(0.2);
            $this->Ln(1);
            break;
        case 'STRONG':
            $this->SetStyle('B', true);
            break;
        case 'EM':
            $this->SetStyle('I', true);
            break;
        case 'B':
        case 'I':
        case 'U':
            $this->SetStyle($tag, true);
            break;
        case 'A':
            $this->HREF=$attr['HREF'];
            break;
        case 'IMG':
            if(isset($attr['SRC']) && (isset($attr['WIDTH']) || isset($attr['HEIGHT']))) {
                if(!isset($attr['WIDTH']))
                    $attr['WIDTH'] = 0;
                if(!isset($attr['HEIGHT']))
                    $attr['HEIGHT'] = 0;
                $this->Image($attr['SRC'], $this->GetX(), $this->GetY(), px2mm($attr['WIDTH']), px2mm($attr['HEIGHT']));
            }
            break;
        case 'BLOCKQUOTE':
        case 'BR':
            $this->Ln(5);
            break;
        case 'P':
            $this->Ln(10);
            break;
        case 'FONT':
            if (isset($attr['COLOR']) && $attr['COLOR']!='') {
                $coul=hex2dec($attr['COLOR']);
                $this->SetTextColor($coul['R'], $coul['G'], $coul['B']);
                $this->issetcolor=true;
            }
            if (isset($attr['FACE']) && in_array(strtolower($attr['FACE']), $this->fontlist)) {
                $this->SetFont(strtolower($attr['FACE']));
                $this->issetfont=true;
            }
            if (isset($attr['FACE']) && in_array(strtolower($attr['FACE']), $this->fontlist) && isset($attr['SIZE']) && $attr['SIZE']!='') {
                $this->SetFont(strtolower($attr['FACE']), '', $attr['SIZE']);
                $this->issetfont=true;
            }
            break;
    }
}

function CloseTag($tag)
{
    //Closing tag
    if($tag=='SUP') {
    }

    if($tag=='TD') { // TD-END
        $this->tdbegin=false;
        $this->tdwidth=0;
        $this->tdheight=0;
        $this->tdalign="L";
        $this->tdbgcolor=false;
    }
    if($tag=='TR') { // TR-END
        $this->Ln();
    }
    if($tag=='TABLE') { // TABLE-END
        $this->tableborder=0;
    }

    if($tag=='STRONG')
        $tag='B';
    if($tag=='EM')
        $tag='I';
    if($tag=='B' || $tag=='I' || $tag=='U')
        $this->SetStyle($tag, false);
    if($tag=='A')
        $this->HREF='';
    if($tag=='FONT'){
        if ($this->issetcolor==true) {
            $this->SetTextColor(0);
        }
        if ($this->issetfont) {
            $this->SetFont('arial');
            $this->issetfont=false;
        }
    }
}

function SetStyle($tag, $enable)
{
    //Modify style and select corresponding font
    $this->$tag+=($enable ? 1 : -1);
    $style='';
    foreach(array('B', 'I', 'U') as $s) {
        if($this->$s>0)
            $style.=$s;
    }
    $this->SetFont('', $style);
}

function PutLink($URL, $txt)
{
    //Put a hyperlink
    $this->SetTextColor(0, 0, 255);
    $this->SetStyle('U', true);
    $this->Write(5, $txt, $URL);
    $this->SetStyle('U', false);
    $this->SetTextColor(0);
}
#===========================================================================
function Footer()
{
    //Position at 1.5 cm from bottom
    $this->SetY(-1);
    //Arial italic 8
    $this->SetFont('helvetica','I',8);
    //Page number
    $this->Cell(0,0.1,'Page '.$this->PageNo().'/{nb}',0,0,'C');
}

#===================================================================================
#untuk membuat kop surat dinas
function letak($gambar){
$this->Image($gambar,26,10,20,25);
}

function judul($teks1, $teks2, $teks3, $teks4, $teks5){
$this->Cell(25);
$this->SetFont('Times','B','15');
$this->Cell(0,8,$teks1,0,1,'C');
$this->Ln(0.5);
$this->Cell(25);
$this->SetFont('Times','B','18');
$this->Cell(0,3,$teks2,0,1,'C');
$this->Ln(0.5);
$this->Cell(25);
$this->SetFont('Times','','10');
$this->Cell(0,5,$teks3,0,1,'C');
$this->Ln(0.5);

$this->Cell(25);
$this->Cell(0,2,$teks4,0,1,'C');

$this->Cell(25);
$this->SetFont('Times','B','13');
$this->Cell(0,6,$teks5,0,1,'C');
$this->Cell(25);
$this->Ln();
}

function garis(){
$this->SetLineWidth(0.85);
$this->Line(25,36,190,36);
$this->SetLineWidth(0);
$this->Line(25,37,190,37);
$this->Ln(5);
}

function Terbilang($x)
    {
        $ambil = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
        if ($x < 12)
            return " " . $ambil[$x];
        elseif ($x < 20)
            return Terbilang($x - 10) . " belas";
        elseif ($x < 100)
            return Terbilang($x / 10) . " puluh" . Terbilang($x % 10);
        elseif ($x < 200)
            return " seratus" . Terbilang($x - 100);
        elseif ($x < 1000)
            return Terbilang($x / 100) . " ratus" . Terbilang($x % 100);
        elseif ($x < 2000)
            return " seribu" . Terbilang($x - 1000);
        elseif ($x < 1000000)
            return Terbilang($x / 1000) . " ribu" . Terbilang($x % 1000);
        elseif ($x < 1000000000)
            return Terbilang($x / 1000000) . " juta" . Terbilang($x % 1000000);
    }
}
//instantisasi objek
$pdf = new pdf();
//Mulai dokumen
$pdf->AddPage();
//meletakkan gambar
$pdf->letak('gambar/kotim.png');
//meletakkan judul disamping logo diatas
$pdf->judul('PEMERINTAH KABUPATEN KOTAWARINGIN TIMUR', 'DINAS KOMUNIKASI DAN INFOMATIKA','Jln. Tjilik Riwut No. 2 Telp. (0531) 2068544 Fax. (0531) 2068545 SAMPIT', 'e-mail: diskominfo@kotimkab.go.id, website: www.kotimkab.go.id','Kalimantan Tengah 74322');
//membuat garis ganda tebal dan tipis
$pdf->garis();

// ===========================================================================================================================
#untuk membuat pembuka surat(dasar surat tugas dan nomor surat)
$pdf->Cell(25);
$pdf->SetFont('Times','B','12');
$pdf->Cell(0,5,'SURAT TUGAS',0,1,'C');
$pdf->Ln(1);
$pdf->SetLineWidth(0.7);
$pdf->Line(102,45,133,45);

$pdf->SetFont('Times','','12');
$pdf->Cell(63);
$pdf->Cell(0,5,'Nomor  : ',0,1,'J');

$pdf->Ln(-4);
$pdf->Cell(45);

$dasar= "SELECT * FROM tmp_surat WHERE no_surat='$noNota'";
$myDasar= mysql_query($dasar, $koneksidb)  or die ("Query salah : ".mysql_error());
while ($kolomDasar = mysql_fetch_array($myDasar)) {
    $pdf->Cell(40);
    $pdf->multiCell(0,4,$kolomDasar['no_surat'],'J');
}

$pdf->Ln(5);
$pdf->SetFont('Times','','12');
$pdf->Cell(7);
$pdf->Cell(0,5,'Dasar         :',0,1,'J');

$pdf->Ln(-5);
$pdf->SetFont('Times','','12');
$pdf->Cell(30);
$pdf->Cell(0,5,'1. ',0,1,'J');
$pdf->Ln(-5);
$pdf->Cell(38);
$pdf->multiCell(0,5,'Peraturan Menteri Keuangan Nomor 113/PMK.05/2012 tentang Penjelasan Dinas Dalam Negeri Bagi Pejabat Negeri, Pegawai Negeri dan Pegawai Tidak Tetap;','J');
$pdf->Cell(30);
$pdf->Cell(0,5,'2. ',0,1,'J');
$pdf->Ln(-4.5);
$pdf->Cell(38);
$pdf->multiCell(0,4,'Peraturan Bupati Kotawaringin Timur Nomor 44 Tahun 2014 tentang Perjalanan Dinas Dalam Negeri dan Bagi Pejabat Negera, Pegawai Negeri, dan Pegawai Tidak di Lingkungan Pemerintah Kabupaten Kotawaringin Timur, sebagaimana telah diubah dengan Peraturan Bupati Kotawaringin Timur Nomor 49 Tahun 2015 tentang Perubahan atas Peraturan Bupati Kotawaringin Timur Nomor 44 Tahun 2014 tentang Perjalanan Dinas dalam Negeri bagi Pejabat Negeri, Pegawai Negeri Sipil dan Pegawai Tidak Tetap di Lingkungan Pemerintah Kabupaten Kotawaringin Timur.','J');

$pdf->Cell(32);
$dasar= "SELECT * FROM dasar_perjalanan WHERE no_surat='$noNota'";
$myDasar= mysql_query($dasar, $koneksidb)  or die ("Query salah : ".mysql_error());
$nomor=2;
while ($kolomDasar = mysql_fetch_array($myDasar)) {
    $nomor++;
    $pdf->cell(0,4,$nomor .'. ',0,1,'J');
    $pdf->Ln(-4.5);
    $pdf->Cell(38);
    $pdf->multiCell(0,4,$kolomDasar['dasar'],'J');
}

$pdf->Ln(3);
$pdf->Cell(25);
$pdf->SetFont('Times','B','12');
$pdf->Cell(0,5,'MENUGASKAN',0,1,'C');
$pdf->Ln(3);

$pdf->Cell(7);
$pdf->SetFont('Times','','12');
$pdf->Cell(0,5,'Kepada      :',0,1,'J');

// $pdf->Cell(35);
$pdf->Ln(-4);
$dasar= "SELECT * FROM perjalanan_pegawai WHERE no_surat='$noNota'";
$myDasar= mysql_query($dasar, $koneksidb)  or die ("Query salah : ".mysql_error());
$nomor=0;
while ($kolomDasar = mysql_fetch_array($myDasar)) {
    $nomor++;
    $pdf->Cell(30);
    $pdf->cell(0,4,$nomor .'. ',0,1,'J');
    $pdf->Ln(-4);
    $pdf->Cell(38);
    $pdf->multiCell(0,4,'Nama                           :  ','J');
    $pdf->Ln(-4);
    $pdf->Cell(83);
    $pdf->multiCell(0,4,$kolomDasar['nama_pegawai'],'J');

    $pdf->Cell(38);
    $pdf->multiCell(0,4,'NIP.                             :','J');
    $pdf->Ln(-4);
    $pdf->Cell(83);
    $pdf->multiCell(0,4,$kolomDasar['nip'],'J');

    $pdf->Cell(38);
    $pdf->multiCell(0,4,'Pangkat/Golongan       : ','J');
    $pdf->Ln(-4);
    $pdf->Cell(83);
    $pdf->multiCell(0,4,$kolomDasar['pangkat'].'/'.$kolomDasar['golongan'],'J');

    $pdf->Cell(38);
    $pdf->multiCell(0,4,'Jabatan                         :','J');
    $pdf->Ln(-4);
    $pdf->Cell(83);
    $pdf->multiCell(0,4,$kolomDasar['jabatan'],'J');
    $pdf->Ln(1);
}

$pdf->Ln(4);
$pdf->Cell(7);
$pdf->SetFont('Times','','12');
$pdf->Cell(0,5,'Untuk        :',0,1,'J');

$pdf->Ln(-4.5);
$dasar= "SELECT * FROM tmp_surat WHERE no_surat='$noNota'";
$myDasar= mysql_query($dasar, $koneksidb)  or die ("Query salah : ".mysql_error());
// $nomor=0;
while ($kolomDasar = mysql_fetch_array($myDasar)) {
    // $nomor++;
    // $pdf->Cell(30);
    // $pdf->cell(0,4,$nomor .'. ',0,1,'J');
    // $pdf->Ln(-4);
    $pdf->Cell(38);
    $pdf->multiCell(0,4,$kolomDasar['tujuan'],'J');
}


$pdf->Ln(4);
$pdf->Cell(38);
$pdf->SetFont('Times','','12');
$pdf->Cell(0,5,'Dengan ketentuan sebagai berikut :',0,1,'J');

$dasar= "SELECT * FROM tmp_surat WHERE no_surat='$noNota'";
$myDasar= mysql_query($dasar, $koneksidb)  or die ("Query salah : ".mysql_error());
$nomor=0;

while ($kolomDasar = mysql_fetch_array($myDasar)) {
    $angka = $kolomDasar['lama_jalan'];
    $bilang= $pdf->Terbilang($angka);

    $nomor++;
    $pdf->Cell(38);
    $pdf->cell(0,4,$nomor .'. ',0,1,'J');

    $pdf->Ln(-4);
    $pdf->Cell(44);
    $pdf->multiCell(0,4,'Lama Penugasan selama '.$kolomDasar['lama_jalan'].' ('.$bilang.') hari mulai tanggal '.$kolomDasar['tgl_berangkat'].' s/d '.$kolomDasar['tgl_kembali'],'J');
}

    $pdf->Cell(38);
    $pdf->cell(0,4,'2. ',0,1,'J');
    $pdf->Ln(-4);
    $pdf->Cell(44);
    $pdf->multiCell(0,4,'Melapor kepada Pejabat setempat mengenai pelaksanaan tugas tersebut','J');

    $pdf->Cell(38);
    $pdf->cell(0,4,'3. ',0,1,'J');
    $pdf->Ln(-4);
    $pdf->Cell(44);
    $pdf->multiCell(0,4,'Melapor hasil pelaksanaan tugas kepada Pejabat yang memberi tugas','J');

    $pdf->Cell(38);
    $pdf->cell(0,4,'4. ',0,1,'J');
    $pdf->Ln(-4);
    $pdf->Cell(44);
    $pdf->multiCell(0,4,'Perintah Ini dilaksanakan dengan penuh tanggungjawab','J');

    $pdf->Cell(38);
    $pdf->cell(0,4,'5. ',0,1,'J');
    $pdf->Ln(-4);
    $pdf->Cell(44);
    $pdf->multiCell(0,4,'Apabila terdapat kekeliruan dalam Surat Tugas ini akan diadakan perbaikan sebagaimana mestinya.','J');

    $pdf->Ln(20);
    $pdf->Cell(118);
    $pdf->cell(0,4,'Dikeluarkan di :',0,1,'J');

    $dasar= "SELECT * FROM tmp_surat WHERE no_surat='$noNota'";
    $myDasar= mysql_query($dasar, $koneksidb)  or die ("Query salah : ".mysql_error());
    while ($kolomDasar = mysql_fetch_array($myDasar)) {
    $pdf->Ln(-4);
    $pdf->Cell(150);
    $pdf->multiCell(0,4,$kolomDasar['tempat_berangkat'],'J');
}

    $pdf->Cell(118);
    $pdf->cell(0,4,'Tanggal            :',0,1,'J');
    $pdf->Ln(-4);
    $array_hari = array(1=>"Senin","Selasa","Rabu","Kamis","Jumat", "Sabtu","Minggu");
    $hari = $array_hari[date("N")];

    $tanggal = date ("j");

    $array_bulan = array(1=>"Januari","Februari","Maret", "April", "Mei", "Juni","Juli","Agustus","September","Oktober", "November","Desember");
    $bulan = $array_bulan[date("n")];

    $tahun = date("Y");

    $pdf->Cell(155);
    $pdf->multiCell(0,4,$bulan.' '.$tahun,'J');

    $pdf->Ln(10);
    $pdf->Cell(108);
    $pdf->cell(0,4,'KELAPA DINAS,',0,1,'C');

    $dasar= "SELECT tmp_surat.* , pegawai.* FROM tmp_surat, pegawai WHERE no_surat='$noNota' AND tmp_surat.pemberi_tugas=pegawai.jabatan";
    $myDasar= mysql_query($dasar, $koneksidb)  or die ("Query salah : ".mysql_error());
    while ($kolomDasar = mysql_fetch_array($myDasar)) {
        if ($kolomDasar['jabatan']=='kepala dinas' || $kolomDasar['pemberi_tugas']=='KEPALA DINAS') { } 
        else{
            $pdf->Cell(108);
            $pdf->Cell(0,4,'Mewakili',0,1,'C'); }
        $pdf->Ln(20);
        $pdf->Cell(108);
        $pdf->SetFont('Times','B','12');
        $pdf->Cell(0,4,$kolomDasar['nama_pegawai'],0,1,'C');
        $pdf->Cell(108);
        $pdf->SetFont('Times','','12');
        $pdf->Cell(0,4,$kolomDasar['pangkat'].'/'.$kolomDasar['golongan'],0,1,'C');
        $pdf->Cell(108);
        $pdf->Cell(0,4,$kolomDasar['nip'],0,1,'C');
    }

$pdf->Ln(6);
$pdf->Cell(7);
$pdf->Cell(0,5,'Tembusan disampaikan kepada Yth : :',0,1,'J');

$pdf->Cell(7);
$pdf->Cell(0,5,'1. Pejabat Penatausahaan Keuangan Diskominfo Kab. Kotim',0,1,'J');
$pdf->Cell(7);
$pdf->Cell(0,5,'2. Atasan langsung dari Pelaksana Perjalanan Dinas',0,1,'J');
$pdf->Cell(7);
$pdf->Cell(0,5,'3. Bendaharawan Pengeluaran Diskominfo Kab. Kotim',0,1,'J');

#==============================================================================================================================
#==============================================================================================================================

$pdf->AddPage();
$pdf->SetFont('Times','B','11');
$pdf->Ln(10);
$pdf->Cell(1);
$pdf->multiCell(108,5,'PEMERINTAH KABUPATEN KOTAWARINGIN TIMUR DINAS KOMUNIKASI DAN INFOMATIKA',1,'C');

// $html='<table border="1">
// <tr>
// <td width="200" height="30" bgcolor="#D0D0FF">PEMERINTAH KABUPATEN KOTAWARINGIN TIMUR
// DINAS KOMUNIKASI</td>
// </tr>
// </table>';
// $pdf->WriteHTML($html);
    $pdf->Ln(-10);
    $pdf->SetFont('Times','','12');
    $pdf->Cell(120);
    $pdf->cell(0,4,'Lembar ke    :  I, II, III, IV, V, VI',0,1,'J');
    $pdf->Cell(120);
    $pdf->cell(0,4,'Kode No       : ',0,1,'J');
    $pdf->Cell(120);
    $pdf->cell(0,4,'Nomor          : ',0,1,'J');
    $pdf->Ln(-4);
    $pdf->Cell(120);
    $dasar= "SELECT * FROM tmp_surat WHERE no_surat='$noNota'";
    $myDasar= mysql_query($dasar, $koneksidb)  or die ("Query salah : ".mysql_error());
    while ($kolomDasar = mysql_fetch_array($myDasar)) {
    $pdf->Cell(25);
    $pdf->multiCell(0,4,$kolomDasar['no_surat'],'J');
}
    $pdf->Ln(8);
    $pdf->Cell(10);
    $pdf->SetFont('Times','B','12');
    $pdf->Cell(0,5,'SURAT PERJALANAN DINAS',0,1,'C');
    $pdf->SetLineWidth(0.5);
    $pdf->Line(81,44.5,139,44.5);
    $pdf->Ln(4);
    $pdf->SetFont('Times','','12');
    $pdf->Cell(1);
    $pdf->multiCell(89,5,'1.  Pejabat yang berwenang memberikan perintah                ',1,'J');
    $pdf->Ln(-10);
    $pdf->Cell(90);
    $pdf->multiCell(0,5,'KEPALA DINAS KOMUNIKASI DAN INFOMATIKA KABUPATEN KOTAWARINGIN TIMUR',1,'J');

    $pdf->Cell(1);
    $pdf->multiCell(89,5,'2.  Nama/NIP pegawai yang melaksanakan                   perjalanan dinas',1,'J');

    $pdf->Ln(-10);
    $dasar= "SELECT * FROM perjalanan_pegawai WHERE no_surat='$noNota'";
    $myDasar= mysql_query($dasar, $koneksidb)  or die ("Query salah : ".mysql_error());
    $nomor=0;
    while ($kolomDasar = mysql_fetch_array($myDasar)) {

        $nomor++;
        // $pdf->Cell(90);
        // $pdf->cell(0,4,$nomor .'. ',0,1,'J');
        // $pdf->Ln(-4);
        
        $pdf->Cell(90);
        $pdf->multiCell(0,5,$nomor.'.  '.$kolomDasar['nama_pegawai'].'/'.$kolomDasar['nip'],'J');
        $pdf->Ln(1);
    }
    $pdf->Ln(2);
    $pdf->Cell(1);
    $pdf->multiCell(86,5,'3.   a. Pangkat dan Golongan',1,'J');
    $pdf->Ln(-4);

    $dasar= "SELECT * FROM perjalanan_pegawai WHERE no_surat='$noNota'";
    $myDasar= mysql_query($dasar, $koneksidb)  or die ("Query salah : ".mysql_error());
    $nomor=0;
    while ($kolomDasar = mysql_fetch_array($myDasar)) {
        $nomor++;
        
        $pdf->Cell(90);
        $pdf->multiCell(0,4,'a. '.$nomor.'.  '.$kolomDasar['pangkat'].'/'.$kolomDasar['golongan'],'J');
        $pdf->Ln(1);
    }
    $pdf->Cell(1);
    $pdf->multiCell(86,5,'      b.  Jabatan/Instansi',1,'J');
    $pdf->Ln(-4);

    $dasar= "SELECT * FROM perjalanan_pegawai WHERE no_surat='$noNota'";
    $myDasar= mysql_query($dasar, $koneksidb)  or die ("Query salah : ".mysql_error());
    $nomor=0;
    while ($kolomDasar = mysql_fetch_array($myDasar)) {
        $nomor++;
        
        $pdf->Cell(90);
        $pdf->multiCell(0,4,'b. '.$nomor .'.  '.$kolomDasar['pangkat'].'/'.$kolomDasar['golongan'] ,'J');
        $pdf->Ln(1);
    }
    $pdf->Ln(2);
    $pdf->Cell(1);
    $pdf->multiCell(86,5,'      c.  Tingkat Biaya Perjalanan Dinas',1,'J');
    $pdf->Ln(-4);

    $dasar= "SELECT * FROM tmp_surat WHERE no_surat='$noNota'";
    $myDasar= mysql_query($dasar, $koneksidb)  or die ("Query salah : ".mysql_error());
    $nomor=0;
    while ($kolomDasar = mysql_fetch_array($myDasar)) {
        $nomor++;
        
        $pdf->Cell(90);
        $pdf->multiCell(0,4,'c. '.$nomor .'.  '.$kolomDasar['tingkat_biaya_p'],'J');
        $pdf->Ln(1);
    }

    $pdf->Ln(4);
    $pdf->Cell(1);
    $pdf->multiCell(86,5,'4.   Maksud Perjalanan Dinas',1,'J');
    $pdf->Ln(-4);

    $dasar= "SELECT * FROM tmp_surat WHERE no_surat='$noNota'";
    $myDasar= mysql_query($dasar, $koneksidb)  or die ("Query salah : ".mysql_error());
    $nomor=0;
    while ($kolomDasar = mysql_fetch_array($myDasar)) {
        $nomor++;
        $pdf->Cell(90);
        $pdf->multiCell(0,4,$kolomDasar['tujuan'],'J');
        $pdf->Ln(1);
    }

#=======================================================================================================================
#=======================================================================================================================

// $pdf->AddPage();
// //untuk judul table, bisa diganti


// $pdf->Ln(8);
//     $pdf->Cell(10);
//     $pdf->SetFont('Times','B','12');
//     $pdf->Cell(0,5,'SURAT PERJALANAN DINAS',0,1,'C');
//     $pdf->SetLineWidth(0.5);
//     $pdf->Line(81,22.5,139,22.5);
//     $pdf->Ln(4);

// $pdf->setFont('Arial','B',8);
// $pdf->setXY(80,10); 
// $pdf->cell(30,6,'Data Statistik LabHouse');
// $pdf->setFont('Arial','B',10);
// $y_initial = 31;
// $y_axis1 = 25;
// $no = 0;
// $row = 6;
// $pdf->setFont('Arial','',8);
// $pdf->setFillColor(233,233,233);
// $pdf->setY($y_axis1);
// $pdf->setX(25);
// //untuk nama field, bisa diganti
// $pdf->cell(10,6,'No',1,0,'C',1);
// $pdf->cell(50,6,'Nama',1,0,'C',1);
// $pdf->cell(20,6,'Jabatan',1,0,'C',1);
// $y = $y_initial + $row;
// //untuk query database, bisa diganti
// $sqp="SELECT * FROM perjalanan_pegawai where no_surat='$noNota' ";

// $nilai = mysql_query($sqp,$koneksidb);

// //untuk menampilkan data, bisa diganti
// while ($daftar = mysql_fetch_array($nilai))
// {
// $no++;
// $pdf->setY($y);
// $pdf->setX(25);
// $pdf->cell(10,6,$no,1,0,'C');
// $pdf->cell(50,6,$daftar['nama_pegawai'],1,0,'L');
// $pdf->multicell(20,6,$daftar['jabatan'],1,0,'C');
// $y = $y + $row;
// }

$pdf->Output('surattugas.pdf','I');
?>