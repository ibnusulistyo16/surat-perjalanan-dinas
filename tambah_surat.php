<?php
error_reporting("E_ALL^E_NOTICE");
// session_start();
if (!defined('BASEPATH')) exit('No direct script access allowed');
include_once "koneksi.php";

# =============================================================
#untuk menambah data pegawai yang akan berangkat
if (isset($_POST['btnTambah'])){

    $id = $_POST['id'];
    $_SESSION['id']=$id;

    $no_surat   = $_POST['no_surat'];
    $_SESSION['no_surat']=$no_surat;

    $tingkat_biaya_p  = $_POST['tingkat_biaya_p'];
    $_SESSION['tingkat_biaya_p']=$tingkat_biaya_p;

    $tujuan = $_POST['tujuan'];
    $_SESSION['tujuan']=$tujuan;

    $alat_angkut  = $_POST['alat_angkut'];
    $_SESSION['alat_angkut']=$alat_angkut;

    $tempat_berangkat   = $_POST['tempat_berangkat'];
    $_SESSION['tempat_berangkat']= $tempat_berangkat;

    $tempat_tujuan   = $_POST['tempat_tujuan'];
    $_SESSION['tempat_tujuan']=$tempat_tujuan;

    $lama_jalan   = $_POST['lama_jalan'];
    $_SESSION['lama_jalan']=$lama_jalan;

    $tgl_berangkat   =  mysql_real_escape_string($_POST['tgl_berangkat']);
    $_SESSION['tgl_berangkat']=$tgl_berangkat;

    $tgl_kembali   =  mysql_real_escape_string($_POST['tgl_kembali']);
    $_SESSION['tgl_kembali']=$tgl_kembali;

    $instansi   = $_POST['instansi'];
    $_SESSION['instansi']=$instansi;

    $akun   = $_POST['akun'];
    $_SESSION['akun']=$akun;

    $lain   = $_POST['lain'];
    $_SESSION['lain']=$lain;

    $pemberi_tugas = $_POST['pemberi_tugas'];
    $_SESSION['pemberi_tugas']=$pemberi_tugas;

    $nip1 = $_POST['nip'];
    $nama_pegawai1  = $_POST['nama_pegawai'];
    $pangkat1 = $_POST['pangkat'];
    $golongan1  = $_POST['golongan'];
    $jabatan1 = $_POST['jabatan'];
    # Validasi Form
    $pesanError = array();
    if (trim($no_surat)=="") {
        $pesanError[]="Data <b>Nomor surat belum di isi</b>";
      }  
    if (trim($nip1)=="") {
      $pesanError[] = "Data <b>nip belum di isi</b>";   
    }
    if (trim($nama_pegawai1)=="") {
      $pesanError[] = "Data <b>nama pegawai (Qty) belum diisi</b>!";    
    }

    if (count($pesanError)>=1 ){
      echo "<div class='mssgBox'>";
        $noPesan=0;
        foreach ($pesanError as $indeks=>$pesan_tampil) { 
        $noPesan++;
          echo "&nbsp;&nbsp; $noPesan. $pesan_tampil<br>";  
    } 
      echo "</div> <br>"; 
    }
    else 
    {
    # SIMPAN KE DATABASE (tmp_suratP)  
    // Jika Kode ditemukan, masukkan data ke Keranjang (TMP)    
      {
      $tmpSql = "INSERT INTO tmp_suratp (id, no_surat, nip, nama_pegawai, pangkat,golongan,jabatan) 
          VALUES ('$id','$no_surat', '$nip1', '$nama_pegawai1', '$pangkat1', '$golongan1', '$jabatan1')";
        mysql_query($tmpSql, $koneksidb) or die ("Gagal Query tmp : ".mysql_error());
      }
    }
}
#-==========================================
# UNTUK MENYIMPAN SELURUH DATA PERJALANAN. 
if(isset($_POST['btnSimpan']))
{	# Baca Variabel from
	$no_surat= $_POST['no_surat'];
  $tingkat_biaya_p	= $_POST['tingkat_biaya_p'];
 	$tujuan	= $_POST['tujuan'];
  $alat_angkut	= $_POST['alat_angkut'];
  $tempat_berangkat   = $_POST['tempat_berangkat'];
  $tempat_tujuan   = $_POST['tempat_tujuan'];
  $lama_jalan   = $_POST['lama_jalan'];
  $tgl_berangkat   = mysql_real_escape_string($_POST['tgl_berangkat']);
  $tgl_kembali   = mysql_real_escape_string($_POST['tgl_kembali']);
  $instansi   = $_POST['instansi'];
  $akun   = $_POST['akun'];
  $lain   = $_POST['lain'];
  $pemberi_tugas = $_POST['pemberi_tugas'];
  $dasar = $_POST['dasar'];

	# Validasi Form
	$pesanError = array();
	if (trim($no_surat)=="") {
		$pesanError[] = "Data <b>Nomor Surat </b> tidak boleh kosong !";		
	}
	if (trim($tingkat_biaya_p)=="") {
		$pesanError[] = "Data <b>tingkat biaya perjalanan dinas</b> tidak boleh kosong!";		
	}
	if (trim($tujuan)=="") {
		$pesanError[] = "Data <b>Maksud Tujuan perjalanan dinas</b> tidak boleh kosong!";		
	}
	if (trim($alat_angkut)=="") {
		$pesanError[] = "Data <b>Alat Angkut yang digunakan</b> tidak boleh kosong!";		
	}
	if (trim($tempat_berangkat)=="") {
		$pesanError[] = "Data <b>Tempat Berangkat dinas</b> belum ada yang dipilih !";		
	}
	if (trim($tempat_tujuan)=="") {
		$pesanError[] = "Data <b>Tempat Tujuan Perjalanan dinas</b> tidak boleh kosong!";		
	}
	if (trim($lama_jalan)=="") {
		$pesanError[] = "Data <b>Lama Perjalanan Dinas</b> tidak boleh kosong!";		
	}
	if (trim($instansi)=="") {
		$pesanError[] = "Data <b>Instansi pembiaya perjalanan</b> tidak boleh kosong!";		
	}

  //UNTUK PENGECEKAN DATA AGAR TIDA DUPLIKAT NOMOR SURAT
	$cekdata="select no_surat from tmp_surat where no_surat='$no_surat'";
  $ada=mysql_query($cekdata) or die(mysql_error());
  if(mysql_num_rows($ada)>0){
    $pesanError[] = "Data <b>Nomor Surat sudah ada !</b> !";    
  }
	// JIKA ADA PESAN ERROR DARI VALIDASI
	if (count($pesanError)>=1 ){
		echo "<div class='mssgBox'>";
			$noPesan=0;
			foreach ($pesanError as $indeks=>$pesan_tampil) { 
			$noPesan++;
				echo "&nbsp;&nbsp; $noPesan.$pesan_tampil<br>";	
			} 
		echo "</div> <br>"; 
	}
    # SIMPAN DATA KE DATABASE tmp_surat. // Jika tidak menemukan error, simpan data ke database..
      #INGAT catatan ini akan diubah ke database baru yaitu ke tabel surat
    $mySql  = "INSERT INTO tmp_surat(no_surat, tingkat_biaya_p, tujuan, alat_angkut, tempat_berangkat, tempat_tujuan, lama_jalan, tgl_berangkat, tgl_kembali, instansi, akun, lain, pemberi_tugas) 
      VALUES ('$no_surat', 
              '$tingkat_biaya_p', 
              '$tujuan', 
              '$alat_angkut', 
              '$tempat_berangkat', 
              '$tempat_tujuan', 
              '$lama_jalan', 
              '$tgl_berangkat', 
              '$tgl_kembali', 
              '$instansi', 
              '$akun', 
              '$lain',
              '$pemberi_tugas')";
              
    $myQry  = mysql_query($mySql, $koneksidb) or die ("Gagal query".mysql_error());



    $mySql1  = "INSERT INTO dasar_perjalanan(id,no_surat, dasar) 
      VALUES ('$id','$no_surat','$dasar')";
              
    $myQry  = mysql_query($mySql1, $koneksidb) or die ("Gagal query".mysql_error());

   # …LANJUTAN, SIMPAN DATA
    # Ambil semua data barang yang dipilih, berdasarkan kasir yg login
    $tmpSql ="SELECT * FROM tmp_suratp";
    $tmpQry = mysql_query($tmpSql, $koneksidb) or die ("Gagal Query Tmp".mysql_error());
    while ($tmpData = mysql_fetch_array($tmpQry)) {
      // Baca data dari tabel barang dan jumlah yang dibeli dari TMP
      // $dataKode = $tmpData['no_surat'];
      $dataNip  = $tmpData['nip'];
      $dataNm = $tmpData['nama_pegawai'];
      $dataPkt  = $tmpData['pangkat'];
      $dataGln  = $tmpData['golongan'];
      $dataJbt  = $tmpData['jabatan'];
      // MEMINDAH DATA, Masukkan semua data di atas dari tabel TMP ke tabel ITEM
      $itemSql = "INSERT INTO perjalanan_pegawai SET 
                  no_surat='$no_surat',
                  nip='$dataNip',
                  nama_pegawai='$dataNm',
                  pangkat='$dataPkt', 
                  golongan='$dataGln', 
                  jabatan='$dataJbt'";
      mysql_query($itemSql, $koneksidb) or die ("Gagal Query ".mysql_error());
    # Kosongkan Tmp jika datanya sudah dipindah
    $hapusSql = "DELETE FROM tmp_suratp";
    mysql_query($hapusSql, $koneksidb) or die ("Gagal kosongkan tmp".mysql_error());

    echo "<script>alert('Data pegawai Berhasil di simpan')</script>";
    // echo "<script>";
    // echo "window.open('cetak_surat.php?no_surat=$no_surat', width=12,height=12,left=12, top=25)";
    // echo "</script>";
       echo "<script>";
    echo "window.open('buat_surat.php?no_surat=$no_surat', width=12,height=12,left=12, top=25)";
    echo "</script>";
  }
}


















//  ==================================================================================================
//	=================================================================
if ((isset($_POST['submit1'])) AND ($_POST['nip'] <> "da"))
  {
  $nip1 = $_POST['nip'];
  $_SESSION['nip']=$nip1;

  $sql1 = mysql_query("SELECT * FROM pegawai WHERE nama_pegawai LIKE '%$nip1%' or nip LIKE '%$nip1%'") or die(mysql_error());
  $myData = mysql_fetch_array($sql1); 
  $dataNip= $myData['nip'];
  $dataNama		= $myData['nama_pegawai'];
  $dataPangkat		= $myData['pangkat'];
  $dataGolongan		= $myData['golongan'];
  $dataJabatan		= $myData['jabatan'];

  $no_surat   = $_POST['no_surat'];
  $_SESSION['no_surat']=$no_suratl;

  $tingkat_biaya_p  = $_POST['tingkat_biaya_p'];
  $_SESSION['tingkat_biaya_p']=$tingkat_biaya_p;

  $tujuan = $_POST['tujuan'];
  $_SESSION['tujuan']=$tujuan;

  $alat_angkut  = $_POST['alat_angkut'];
  $_SESSION['alat_angkut']=$alat_angkut;

  $tempat_berangkat   = $_POST['tempat_berangkat'];
  $_SESSION['tempat_berangkat']= $tempat_berangkat;

  $tempat_tujuan   = $_POST['tempat_tujuan'];
  $_SESSION['tempat_tujuan']=$tempat_tujuan;

  $lama_jalan   = $_POST['lama_jalan'];
  $_SESSION['lama_jalan']=$lama_jalan;

  $tgl_berangkat   = $_POST['tgl_berangkat'];
  $_SESSION['tgl_berangkat']=$tgl_berangkat;

  $tgl_kembali   = $_POST['tgl_kembali'];
  $_SESSION['tgl_kembali']=$tgl_kembali;

  $instansi   = $_POST['instansi'];
  $_SESSION['instansi']=$instansi;

  $akun   = $_POST['akun'];
  $_SESSION['akun']=$akun;

  $lain   = $_POST['lain'];
  $_SESSION['lain']=$lain;

  $pemberi_tugas = $_POST['pemberi_tugas'];
  $_SESSION['pemberi_tugas']=$pemberi_tugas;
  }
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>Autocomplete dari database dengan jQuery dan PHP  </title>
    <link rel="stylesheet" href="js/jquery-ui.css" />
    <script src="js/jquery-ui.js"></script>

    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/ilmudetil.css">
    <link rel="stylesheet" href="assets/css/bootstrap-datetimepicker.css"/>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/moment-with-locales.js"></script>
    <script src="assets/js/jquery-1.11.3.min.js"></script>
    <script src="assets/js/bootstrap-datetimepicker.js"></script>

   <!--  <link rel="stylesheet" type="text/css" href="plugins/tigra_calendar/tcal.css"/>
    <script type="text/javascript" src="plugins/tigra_calendar/tcal.js"></script> -->

</head>

<body>   
<form action="<?php $_SERVER['PHP_SELF']; ?>" method="post">   
  <table align="left" class="table-striped table-bordered table-condensed">
    <tr>
      <td colspan="2" bgcolor="#CCCCCC"><strong>DATA PERJALANAN </strong></td>
      <td width="280">&nbsp;</td>
    </tr>
     <tr>
      <td width="210"><strong>Nomor Surat </strong></td>
      <td width="10"><strong>:</strong></td>
      <td width="280"><b>
        <input name="no_surat" value="<?php echo $no_surat; ?>" type="text" class="form-control" style="width: 186px;" />
      </b></td>
    </tr>
    <tr>
    <td><strong>Pejabat yang menanda tangan surat</strong></td>
    <td><b>:</b></td>
    <td>
            <select name="pemberi_tugas" class="form-control" style="width: 180px;" >
             <option value="<?php echo $pemberi_tugas;?>" selected="selected"><?php echo $tingkat_biaya_p;?></option>
              <option value="KEPALA DINAS">KEPALA DINAS</option>
              <option value="SEKRETARIS">SEKRETARIS</option>
              <option value="KEPALA BIDANG PTI">KEPALA BIDANG PTI</option>
              <option value="KEPALA BIDANG PIK">KEPALA BIDANG PIK</option>
            </select>
      </td>
    </tr>



    <tr>
    <td><strong>Tingkat Biaya Perjalanan</strong></td>
    <td><b>:</b></td>
    <td>
            <select name="tingkat_biaya_p" class="form-control" style="width: 126px;" >
             <option value="<?php echo $tingkat_biaya_p;?>" selected="selected"><?php echo $tingkat_biaya_p;?></option>
              <option value="Tingkat A">Tingkat A</option>
              <option value="Tingkat B">Tingkat B</option>
              <option value="Tingkat C">Tingkat C</option>
            </select>
      </td>
    </tr>
    <tr>
      <td width="210"><strong>Maksud Perjalanan Dinas </strong></td>
      <td width="10"><strong>:</strong></td>
      <td width="280"><b>
        <textarea name='tujuan' value="<?php echo $tujuan; ?>" type='text'  class="form-control" style="width: 256px;"><?php echo $tujuan; ?></textarea>
      </b></td>
    </tr>
    <tr>
      <td width="210"><strong>Alat Angkut yang dipergunakan </strong></td>
      <td width="10"><strong>:</strong></td>
      <td width="280"><b>
        <input name="alat_angkut" value="<?php echo $alat_angkut; ?>" type="text"   class="form-control" style="width: 166px;" />
      </b></td>
    </tr>
    <tr>
      <td width="210"><strong>Tempat Berangkat </strong></td>
      <td width="10"><strong>:</strong></td>
      <td width="280"><b>
        <input name="tempat_berangkat" value="<?php echo $tempat_berangkat; ?>"  type="text"  class="form-control" style="width: 166px;" />
      </b></td>
    </tr>
    <tr>
      <td width="210"><strong>Tempat Tujuan</strong></td>
      <td width="10"><strong>:</strong></td>
      <td width="280"><b>
        <input name="tempat_tujuan" value="<?php echo $tempat_tujuan; ?>" type="text"  size="20" class="form-control" style="width: 166px;" />
      </b></td>
    </tr>
<!-- ==================================================================================================== -->

<!--<tr>
      <td><strong>Tanggal Berangkat</strong></td>
      <td><strong>:</strong></td>
      <td><input name="tgl_berangkat" id="tgl1" type="text" id="tanggal" class='tcal' value="<?php echo $tgl_berangkat; ?>" size="20" maxlength="20" /></td>
    </tr>
    <tr>
      <td><strong>Tanggal Kembali</strong></td>
      <td><strong>:</strong></td>
      <td><input name="tgl_kembali" id="tgl2" type="text" class='tcal' value="<?php echo $tgl_kembali; ?>" size="20" maxlength="20" /></td>
    </tr>

    <tr>
      <td width="210"><strong>Lamanya Perjalanan Dinas </strong></td>
      <td width="10"><strong>:</strong></td>
      <td width="280"><b>
        <input name="lama_jalan" value="<?php echo $lama_jalan; ?>" type="text"  size="8" maxlength="25"  />
      </b></td>
    </tr> -->

    <tr>
         <td width="210"><strong>Tanggal Berangkat</strong></td>
         <td width="10" ><strong>:</strong></td>
         <td width="280" class="input-group date" style="width: 212px;" id="tgl_berangkat">
            <input type="text" class="form-control" name="tgl_berangkat" value="<?php echo $tgl_berangkat; ?>"  />  
                <span class="input-group-addon"><span class="glyphicon-calendar glyphicon"></span></span>
         </td>
    </tr>

        <tr>
          <td width="210 "><strong>Tanggal Kembali</strong> </td>
          <td width="10" ><strong>:</strong></td>
            <td width="280" class="input-group date" style="width: 212px;" id="tgl_kembali"  >
              <input type="text" class="form-control"  name="tgl_kembali"  value="<?php echo $tgl_kembali; ?>"/> 
                <span class="input-group-addon"><span class="glyphicon-calendar glyphicon"></span></span>
            </td>
        </tr>

        <tr>
          <td width="210" ><strong>Lama Perjalanan Dinas</strong></td>
          <td width="10" ><strong>:</strong></td>
          <td width="280" ><input type="text"  id="selisih" name="lama_jalan" value="<?php $lama_jalan;?>" class="form-control" style="width: 96px;" />  </td>
        </tr>

<!-- ================================================================================================== -->

	<tr>
      <td colspan="3"><strong>Pembeban Anggaran</strong></td>
    </tr>
    <tr>
      <td width="210"><strong>Instansi : </strong></td>
      <td width="10"><strong>:</strong></td>
      <td width="280" class="input-group date"><b>
        <input name="instansi" value="<?php echo $instansi; ?>" type="text" class="form-control" style="width: 256px;"/>
      </b></td>
    </tr>
    <tr>
      <td width="210"><strong>Akun : </strong></td>
      <td width="10"><strong>:</strong></td>
      <td width="280"><b>
        <input name="akun" value="<?php echo $akun; ?>" type="text" class="form-control" style="width: 256px;"/>
      </b></td>
    </tr>
    <tr>
      <td width="210"><strong>Keterangan Lain-lain : </strong></td>
      <td width="10"><strong>:</strong></td>
      <td width="280"><b>
      <textarea name="lain" value="<?php echo "$lain"; ?>" class="form-control" style="width: 256px;" ><?php echo "$lain"; ?></textarea>
      </b></td>
    </tr>
    <tr>
      <td width="210"><strong>Dasar</strong></td>
      <td width="10"><strong>:</strong></td>
      <td width="280">
      <textarea name="dasar" value= "<?php echo $dasar ?>" class="form-control" style="width: 256px;" > </textarea>
      </td>
    </tr>
    <tr>
      <td colspan="3" bgcolor="#CCCCCC" align="center">
      <input name="btnSimpan" type="submit"  class="btn btn-warning" style="cursor:pointer;" value="SIMPAN"  /></td>
    </tr>
    </table>

<table align="right" class="table-striped table-bordered table-condensed">
  <tr>
    <td colspan="2" bgcolor="#CCCCCC"><strong>INPUT PEGAWAI</strong></td>
    <td width="355">&nbsp;</td>
  </tr>
  	<tr>
      <td width="210"><strong>NIP</strong></td>
      <td width="10"><strong>:</strong></td>
      <td width="355" class="input-group date">
      <input placeholder="Cari data pegawai..." id="appendedInputButton" name="nip" value='<?php echo $dataNip ?>' type="text" class="form-control" style="width: 166px;">
      <button class="btn-default" name="submit1" type="submit " style="padding-bottom: 4.5; padding-top: 4.5;padding-bottom: 4.5;
    padding-bottom: 4,5; padding-bottom: 5px; padding-top: 4.5; padding-top: 5px; ">
<i class="fa fa-search"></i> Cari</button>
      </td>
    </tr>
    <tr>
      <td width="210"><strong>Nama Pegawai</strong></td>
      <td width="10"><strong>:</strong></td>
      <td width="355"><input name='nama_pegawai' value= "<?php echo $dataNama ?>" class="form-control" style="width: 200px;"/></td>
    </tr>
        <tr>
      <td width="210"><strong>Pangkat/ Golongan</strong></td>
      <td width="10"><strong>:</strong></td>
      <td width="355" class="input-group">
      <input name='pangkat' value= "<?php echo $dataPangkat ?>" class="form-control" style="width: 100px;"/>
      <input name='golongan' value= "<?php echo $dataGolongan ?>" class="form-control" style="width: 10px;"/>
      </td>
    </tr>
        <tr>
      <td width="210"><strong>Jabatan</strong></td>
      <td width="10"><strong>:</strong></td>
      <td width="355">
      <textarea name="jabatan" value="<?php echo "$dataJabatan"; ?>" class="form-control" style="width: 256px;" ><?php echo "$dataJabatan"; ?></textarea>
      <!-- <input name='jabatan' value= "<?php echo $dataJabatan ?>" class="form-control" style="width: 200px;"/></td> -->
    </tr>
    <tr>
      <td colspan="3" align="center"><b>
        <input name="btnTambah" class="btn btn-primary" type="submit" style="cursor:pointer;" onclick="return confirm('ANDA YAKIN AKAN MENAMBAHKAN DATA PEGAWAI INI ... ?')"  value=" Tambah " />
      </b></td>
    </tr>
    </table>
 <!-- ==========================================================================================================================    -->
<table align="right" class="table-striped table-bordered table-condensed">
    <tr>
      <th colspan="6">DAFTAR Pegawai </th>
    </tr>
    <tr>
      <td width="1" bgcolor="#CCCCCC"><strong>No</strong></td>
      <td width="97" bgcolor="#CCCCCC"><strong>NIP</strong></td>
      <td width="100" bgcolor="#CCCCCC"><strong>NAMA</strong></td>
      <td width="20" bgcolor="#CCCCCC"><strong>Tool</strong></td>
    </tr>

<?php
// Qury menampilkan data dalam Grid 
$tmpSql ="SELECT  * FROM tmp_suratP  ORDER BY nip ";
$tmpQry = mysql_query($tmpSql, $koneksidb) or die ("Gagal Query Tmp".mysql_error());
$nomor=0;  
while($tmpData = mysql_fetch_array($tmpQry)) {
	$nomor++;
?>
    <tr>
      <td colspan="1" align="right"><?php echo $nomor; ?></td>
      <td width="2"><?php echo $tmpData['nip']; ?></b></td>
      <td width="20"><?php echo $tmpData['nama_pegawai']; ?></td>
      <td width="30"><a href="?page=hapus_tmpsurat&id=<?php echo $tmpData['id'];?>" target="_self" onClick="return confirm('Apakah anda yakin ingin menghapus data ini ?')">Hapus</a></td>
    </tr>
<?php } ?>
</table>
</form>

<script>
$(function() { 
  $('#tgl_berangkat').datetimepicker({
   locale:'id',
   format:'DD MMMM YYYY'    
   });
   
  $('#tgl_kembali').datetimepicker({
   useCurrent: false,
   locale:'id',
   format:'DD MMMM YYYY'
   });
   
   $('#tgl_berangkat').on("dp.change", function(e) {
    $('#tgl_kembali').data("DateTimePicker").minDate(e.date);
  });
  
   $('#tgl_kembali').on("dp.change", function(e) {
    $('#tgl_berangkat').data("DateTimePicker").maxDate(e.date);
      CalcDiff()
   });
  
});

function CalcDiff(){
var a=$('#tgl_berangkat').data("DateTimePicker").date();
var b=$('#tgl_kembali').data("DateTimePicker").date();
    var $timeDiff=0;
     if (b) {
            $timeDiff = (b-a)/ 1000;
        }
  $('#selisih').val(Math.ceil($timeDiff/(86400))+' Hari')   
}
</script> 
</body>
</html>