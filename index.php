<?php
session_start();
include_once "koneksi.php";
// include_once "../config/inc.library.php";
define('BASEPATH','TEST');
// Baca Jam pada Komputer
date_default_timezone_set("Asia/Jakarta");
$dataTanggal 	=  date('d-m-Y');
?>
<!doctype html>
<html lang="en"><head>
    <meta charset="utf-8">
    <title>Aplikasi Surat Perjalanan</title>
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="lib/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="lib/font-awesome/css/font-awesome.css">

    <script src="lib/jquery-1.11.1.min.js" type="text/javascript"></script>

        <script src="lib/jQuery-Knob/js/jquery.knob.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function() {
            $(".knob").knob();
        });
    </script>

    <link rel="stylesheet" type="text/css" href="stylesheets/temahendry.css">
    <link rel="stylesheet" type="text/css" href="stylesheets/hendry.css">

</head>
<body class=" theme-blue">

    <!-- Demo page code -->

    <script type="text/javascript">
        $(function() {
            var match = document.cookie.match(new RegExp('color=([^;]+)'));
            if(match) var color = match[1];
            if(color) {
                $('body').removeClass(function (index, css) {
                    return (css.match (/\btheme-\S+/g) || []).join(' ')
                })
                $('body').addClass('theme-' + color);
            }

            $('[data-popover="true"]').popover({html: true});
            
        });
    </script>
    <style type="text/css">
        #line-chart {
            height:300px;
            width:800px;
            margin: 0px auto;
            margin-top: 1em;
        }
        .navbar-default .navbar-brand, .navbar-default .navbar-brand:hover { 
            color: #fff;
        }
    </style>

    <script type="text/javascript">
        $(function() {
            var uls = $('.sidebar-nav > ul > *').clone();
            uls.addClass('visible-xs');
            $('#main-menu').append(uls.clone());
        });
    </script>

    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="../assets/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">
</div>


    <div class="navbar navbar-default" role="navigation">

        <div class="navbar-collapse collapse" style="height: 1px;">
          <ul id="main-menu" class="nav navbar-nav navbar-right">
          </ul>

        </div>
      </div>
    </div>

    <div class="sidebar-nav">
    <ul>
    <li><a href="#" data-target=".dashboard-menu" class="nav-header" data-toggle="collapse"><i class="fa fa-fw fa-dashboard"></i> Data Master<i class="fa fa-collapse"></i></a></li>
    <li><ul class="dashboard-menu nav nav-list collapse in">
        <li><a href="?page=awal"><span class="fa fa-caret-right"></span> Beranda</a></li>
        <li ><a href="?page=data_pegawai"><span class="fa fa-caret-right"></span> Data Pegawai</a></li>
        <li ><a href="?page=tambah_surat"><span class="fa fa-caret-right"></span> Surat Perjalanan </a></li>
    </ul></li>

    <li><a href="#" data-target=".accounts-menu" class="nav-header collapsed" data-toggle="collapse"><i class="fa fa-fw fa-list-alt"></i> Laporan <i class="fa fa-collapse"></i></a></li>
        <li><ul class="accounts-menu nav nav-list collapse">
           <li ><a href="?page=cetak_surat"><span class="fa fa-caret-right"></span> Laporan Perjalanan Pegawai </a>
           </li>
    </ul></li>
    </ul>
    </div>

    <div class="content">    
           <?php include "switch.php";?>
    <div class="main-content">
    <div class="row"></div>

    <div class="row"></div>
        <footer>
            <hr>
            <p class="pull-right">All rights reserved.</p>
            <p>© <?php echo date("Y");?> <a href="#" target="_blank"></a></p>
          </footer>
      </div>
    </div>


    <script src="lib/bootstrap/js/bootstrap.js"></script>
    <script type="text/javascript">
        $("[rel=tooltip]").tooltip();
        $(function() {
            $('.demo-cancel-click').click(function(){return false;});
        });
    </script> 
</body>
</html>