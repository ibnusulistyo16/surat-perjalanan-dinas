<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
<link rel="stylesheet" type="text/css" href="plugins/tigra_calendar/tcal.css"/>
<link rel="stylesheet" type="text/css" href="style.php">
<script type="text/javascript" src="plugins/tigra_calendar/tcal.js"></script>  
</head>
<body>   
<h1>Pembuatan Surat Perjalanan  </h1>
     
<?php
error_reporting(0);
if (!defined('BASEPATH')) exit('No direct script access allowed');

// $sql= "SELECT nip,nama,pangkat,golongan,jabatan FROM * pegawai whare nip=$nip";
# SKRIP SAAT TOMBOL SIMPAN DIKLIK
if(isset($_POST['btnSimpan'])){
	# Baca Variabel Data Form
  $no_surat		= mysql_real_escape_string($_POST['no_surat']);
  $nip	= mysql_real_escape_string($_POST['nip']);
  $tingkat_biaya_p	= mysql_real_escape_string($_POST['tingkat_biaya_p']);
  $tujuan	= mysql_real_escape_string($_POST['tujuan']);
  $alat_angkut	= mysql_real_escape_string($_POST['alat_angkut']);
  $tempat_berangkat   = mysql_real_escape_string($_POST['tempat_berangkat']);
  $tempat_tujuan   = mysql_real_escape_string($_POST['tempat_tujuan']);
  $lama_jalan   = mysql_real_escape_string($_POST['lama_jalan']);
  $tgl_berangkat   = mysql_real_escape_string($_POST['tgl_berangkat']);
  $tgl_kembali   = mysql_real_escape_string($_POST['tgl_kembali']);
  $pengikut   = mysql_real_escape_string($_POST['pengikut']);
  $instansi   = mysql_real_escape_string($_POST['instansi']);
  $akun   = mysql_real_escape_string($_POST['akun']);
  $lain   = mysql_real_escape_string($_POST['lain']);
	# Validasi form, jika kosong sampaikan pesan error
	$pesanError = array();
	if (trim($no_surat)=="") {
		$pesanError[] = "Data <b>Nomor Surat</b> tidak boleh kosong !";		
	}
	if (trim($nip)=="") {
		$pesanError[] = "Data <b>NIP pegawai</b> tidak boleh kosong !";		
	}
	if (trim($tingkat_biaya_p)=="") {
		$pesanError[] = "Data <b>tingkat biaya perjalanan dinas</b> tidak boleh kosong!";		
	}
	if (trim($tujuan)=="") {
		$pesanError[] = "Data <b>Maksud Tujuan perjalanan dinas</b> tidak boleh kosong!";		
	}
	if (trim($alat_angkut)=="") {
		$pesanError[] = "Data <b>Alat Angkut yang digunakan</b> tidak boleh kosong!";		
	}
	if (trim($tempat_berangkat)=="") {
		$pesanError[] = "Data <b>Tempat Berangkat dinas</b> belum ada yang dipilih !";		
	}
	if (trim($tempat_tujuan)=="") {
		$pesanError[] = "Data <b>Tempat Tujuan Perjalanan dinas</b> tidak boleh kosong!";		
	}
	if (trim($lama_jalan)=="") {
		$pesanError[] = "Data <b>Lama Perjalanan Dinas</b> tidak boleh kosong!";		
	}
	if (trim($tgl_berangkat)=="") {
		$pesanError[] = "Data <b>tanggal berangkat</b> tidak boleh kosong!";		
	}
	if (trim($tgl_kembali)=="") {
		$pesanError[] = "Data <b>tanggal kembali</b> tidak boleh kosong!";		
	}
	if (trim($instansi)=="") {
		$pesanError[] = "Data <b>Instansi pembiaya perjalanan</b> tidak boleh kosong!";		
	}
	
	$cekdata="select no_surat from surat where no_surat='$no_surat'";
	$ada=mysql_query($cekdata) or die(mysql_error());
	if(mysql_num_rows($ada)>0){
				$pesanError[] = "Data <b>Nomor Surat Sudah Ada !</b> belum ada yang dipilih !";		
	}

	# JIKA ADA PESAN ERROR DARI VALIDASI
	if (count($pesanError)>=1 ){
		echo "<div class='mssgBox'>";
		$noPesan=0;
		foreach ($pesanError as $indeks=>$pesan_tampil) { 
			$noPesan++;
			echo "&nbsp;&nbsp; $noPesan.$pesan_tampil<br>";	
			}
		echo "</div> <br>"; 
	}
	else {
		# SIMPAN DATA KE DATABASE. // Jika tidak menemukan error, simpan data ke database

		$mySql = "INSERT INTO `surat`(`no_surat`, `nip`, `tingkat_biaya_p`, `tujuan`, `alat_angkut`, `tempat_berangkat`, `tempat_tujuan`, `lama_jalan`, `tgl_berangkat`, `tgl_kembali`, `pengikut`, `instansi`, `akun`, `lain`) 
						VALUES ('$no_surat','$nip',
								'$tingkat_biaya_p','$tujuan',
								'$alat_angkut','$tempat_berangkat',
								'$tempat_tujuan','$lama_jalan',
								'$tgl_berangkat','$tgl_kembali',	
								'$pengikut','$instansi',
								'$akun','$lain')";
							
		$myQry	= mysql_query($mySql, $koneksidb) or die ("Gagal query".mysql_error());
		if($myQry){
			echo "<script>alert('Data pegawai Berhasil di simpan')</script>";
			echo "<meta http-equiv='refresh' content='0; url=?page=data_pegawai'>";
		}
		exit;
	}
} // Penutup POST
	
	
# VARIABEL DATA UNTUK DIBACA FORM

$dataTanggal 	= isset($_POST['tgl_p']) ? $_POST['tgl_p'] : date('Y-m-d');
?>
<!-- Modal Dialog Contact -->
<form action="<?php $_SERVER['PHP_SELF']; ?>" method="post" >

  <table align="left" class="table-striped table-bordered table-condensed">
    <tr>
      <td colspan="2" bgcolor="#CCCCCC"><strong>DATA PERJALANAN </strong></td>
      <td width="346">&nbsp;</td>
    </tr>
    <tr>
      <td width="210"><strong>Nomor Surat </strong></td>
      <td width="10"><strong>:</strong></td>
      <td width="346"><b>
        <input name="no_surat" type="text"  size="20" maxlength="25"  />
      </b></td>
    </tr>

    <tr>
      <td width="210"><strong>NIP </strong></td>
      <td width="10"><strong>:</strong></td>
      <td width="346"><b>
        <input name="nip" type="text"  size="20" maxlength="25"  /><a href="?page=cari_pegawai"  target="_self">Cari NIP Pegawai </a></td>
      </b></td>
    </tr>
    <tr>
      <td width="210"><strong>Nama pegawai</strong></td>
      <td width="10"><b>:</b></td>
      <td width="346"><b>
        <input name="nama_pegawai" type="text" size="30" maxlength="30"  />
      </b></td>
    </tr>
	<tr>
      <td width="210" ><strong>Pangkat/ Golongan</strong></td>
      <td width="10"><strong>:</strong></td>
      <td width="346"><input name="pangkat"  size="10" type="text" maxlength="20" />
        /
          <input name="golongan" type="text" size="3" maxlength="3" /></td>
    </tr>

    <tr>
      <td width="210"><strong>Jabatan</strong></td>
      <td width="10"><strong>:</strong></td>
      <td width="346"><input name="jabatan"  size="40" type="text" maxlength="200" /></td>
    </tr>

    <tr>
      <td width="210"><strong> Tingkat Biaya Perjalanan </strong></td>
      <td width="10"><strong>:</strong></td>
      <td width="346"><b>
        <input name="tingkat_biaya_p" type="text"  size="20" maxlength="25"  />
      </b></td>
    </tr>

    <tr>
      <td width="210"><strong>Maksud Perjalanan Dinas </strong></td>
      <td width="10"><strong>:</strong></td>
      <td width="346"><b>
        <input name="tujuan" type="text"  size="20" maxlength="25"  />
      </b></td>
    </tr>

    <tr>
      <td width="210"><strong>Alat Angkut yang dipergunakan </strong></td>
      <td width="10"><strong>:</strong></td>
      <td width="346"><b>
        <input name="alat_angkut" type="text"  size="20" maxlength="25"  />
      </b></td>
    </tr>

    <tr>
      <td width="210"><strong>Tempat Berangkat </strong></td>
      <td width="10"><strong>:</strong></td>
      <td width="346"><b>
        <input name="tempat_berangkat" type="text"  size="20" maxlength="25"  />
      </b></td>
    </tr>

    <tr>
      <td width="210"><strong>Tempat Tujuan</strong></td>
      <td width="10"><strong>:</strong></td>
      <td width="346"><b>
        <input name="tempat_tujuan" type="text"  size="20" maxlength="25"  />
      </b></td>
    </tr>

    <tr>
      <td width="210"><strong>Lamanya Perjalanan Dinas </strong></td>
      <td width="10"><strong>:</strong></td>
      <td width="346"><b>
        <input name="no_surat" type="text"  size="20" maxlength="25"  />
      </b></td>
    </tr>


    <tr>
      <td colspan="3"><strong>Pembeban Anggaran</strong></td>
    </tr>
    <tr>
      <td width="210"><strong>Instansi : </strong></td>
      <td width="10"><strong>:</strong></td>
      <td width="346"><b>
        <input name="instansi" type="text"  size="20" maxlength="25"  />
      </b></td>
    </tr>
    <tr>
      <td width="210"><strong>Akun : </strong></td>
      <td width="10"><strong>:</strong></td>
      <td width="346"><b>
        <input name
        ="akun" type="text"  size="20" maxlength="25"  />
      </b></td>
    </tr>

    <tr>
      <td width="210"><strong>Keterangan Lain-lain : </strong></td>
      <td width="10"><strong>:</strong></td>
      <td width="346"><b>
        <input name="lain" type="text"  size="20" maxlength="25"  />
      </b></td>
    </tr>
    </table>


<table align="right" class="table-striped table-bordered table-condensed">
    <tr>
      <td colspan="3"><strong>Pengikut</strong></td>
    </tr>
    <tr>
      <td width="210"><strong>NIP </strong></td>
      <td width="10"><strong>:</strong></td>
      <td width="346"><b>
        <input name="nip" type="text"  size="20" maxlength="25"  />
      </b></td>
    </tr>
    <tr>
      <td width="210"><strong>Nama pegawai</strong></td>
      <td width="10"><b>:</b></td>
      <td width="346"><b>
        <input name="nama_pegawai" type="text" size="30" maxlength="40"  />
      </b></td>
    </tr>
 <tr>
      <td width="210" ><strong>Pangkat/ Golongan</strong></td>
      <td width="10"><strong>:</strong></td>
      <td width="346"><input name="pangkat"  size="10" type="text" maxlength="20" />
        /
          <input name="golongan" type="text" size="3" maxlength="3" /></td>
    </tr>

    <tr>
      <td width="210"><strong>Jabatan</strong></td>
      <td width="10"><strong>:</strong></td>
      <td width="346"><input name="jabatan"  size="40" type="text" maxlength="200" /></td>
    </tr>
  <tr><td>&nbsp;</td>
      <td>&nbsp;</td>
      <td><b>
      <button class="btn btn-primary" name="btnSimpan" type="submit"   ><i class="fa fa-save"></i> Tambah</button>
      </b></td>
    </tr>
</table>
<br>
</form>
</body>
</html>