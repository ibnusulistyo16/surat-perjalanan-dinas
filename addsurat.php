<?php
error_reporting("E_ALL^E_NOTICE");
session_start();
if (!defined('BASEPATH')) exit('No direct script access allowed');
include_once "koneksi.php";
// =========================================================================
# TOMBOL TAMBAH (KODE barang) DIKLIK
if(isset($_POST['btnTambah'])){
	# Baca Data dari Form  
	$_SESSION['pos']=$_POST;
	$id=$_POST['id'];  
	// $no_surat = mysql_real_escape_string($_POST['no_surat']);
    $nip1	= $_POST['nip'];
	$nama_pegawai1	= $_POST['nama_pegawai'];
	$pangkat1	= $_POST['pangkat'];
	$golongan1	= $_POST['golongan'];
	$jabatan1	= $_POST['jabatan'];
	# Validasi Form
	$pesanError = array();	
	if (trim($nip1)=="") {
		$pesanError[] = "Data <b>nip belum di isi</b>";		
	}
	if (trim($nama_pegawai1)=="") {
		$pesanError[] = "Data <b>nama pegawai (Qty) belum diisi</b>!";		
	}
	if(trim($pangkat1)=="") {
		$pesanError[] = "Data <b>pangkat</b> belum diisi  !";		
	}
	if(trim($golongan1)=="") 
	{
		$pesanError[] = "Data <b>golongan</b> belum diisi  !";		
	}
	if(trim($jabatan1)=="") 
	{
		$pesanError[] = "Data <b>jabatan</b> belum diisi  !";		
	}
	
	# JIKA ADA PESAN ERROR DARI VALIDASI
	if (count($pesanError)>=1 ){
		echo "<div class='mssgBox'>";
			$noPesan=0;
			foreach ($pesanError as $indeks=>$pesan_tampil) { 
			$noPesan++;
				echo "&nbsp;&nbsp; $noPesan. $pesan_tampil<br>";	
			} 
		echo "</div> <br>"; 
	}
	else 
	{
		# SIMPAN KE DATABASE (tmp_penjualan)	
		// Jika Kode ditemukan, masukkan data ke Keranjang (TMP)		
		{
			$tmpSql = "INSERT INTO tmp_suratp (id, no_surat, nip, nama_pegawai, pangkat,golongan,jabatan) 
					VALUES ('$id','$no_surat', '$nip1', '$nama_pegawai1', '$pangkat1', '$golongan1', '$jabatan1')";
		    mysql_query($tmpSql, $koneksidb) or die ("Gagal Query tmp : ".mysql_error());
		}	}
}
# ========================================================================================================
# JIKA TOMBOL SIMPAN TRANSAKSI DIKLIK
if(isset($_POST['btnSimpan']))
{	# Baca Variabel from
	$id = $_POST['id'];
	$no_surat		= $_POST['no_surat'];
  	$tingkat_biaya_p	= $_POST['tingkat_biaya_p'];
 	$tujuan	= $_POST['tujuan'];
  	$alat_angkut	= $_POST['alat_angkut'];
  	$tempat_berangkat   = $_POST['tempat_berangkat'];
  	$tempat_tujuan   = $_POST['tempat_tujuan'];
  	$lama_jalan   = $_POST['lama_jalan'];
  	$tgl_berangkat   = $_POST['tgl_berangkat'];
  	$tgl_kembali   = $_POST['tgl_kembali'];
  	$pengikut   = $_POST['pengikut'];
  	$instansi   = $_POST['instansi'];
  	$akun   = $_POST['akun'];
  	$lain   = $_POST['lain'];
			
	# Validasi Form
	$pesanError = array();
	if (trim($no_surat)=="") {
		$pesanError[] = "Data <b>Nomor Surat</b> tidak boleh kosong !";		
	}
	if (trim($tingkat_biaya_p)=="") {
		$pesanError[] = "Data <b>tingkat biaya perjalanan dinas</b> tidak boleh kosong!";		
	}
	if (trim($tujuan)=="") {
		$pesanError[] = "Data <b>Maksud Tujuan perjalanan dinas</b> tidak boleh kosong!";		
	}
	if (trim($alat_angkut)=="") {
		$pesanError[] = "Data <b>Alat Angkut yang digunakan</b> tidak boleh kosong!";		
	}
	if (trim($tempat_berangkat)=="") {
		$pesanError[] = "Data <b>Tempat Berangkat dinas</b> belum ada yang dipilih !";		
	}
	if (trim($tempat_tujuan)=="") {
		$pesanError[] = "Data <b>Tempat Tujuan Perjalanan dinas</b> tidak boleh kosong!";		
	}
	if (trim($lama_jalan)=="") {
		$pesanError[] = "Data <b>Lama Perjalanan Dinas</b> tidak boleh kosong!";		
	}
	if (trim($tgl_berangkat)=="") {
		$pesanError[] = "Data <b>tanggal berangkat</b> tidak boleh kosong!";		
	}
	if (trim($tgl_kembali)=="") {
		$pesanError[] = "Data <b>tanggal kembali</b> tidak boleh kosong!";		
	}
	if (trim($instansi)=="") {
		$pesanError[] = "Data <b>Instansi pembiaya perjalanan</b> tidak boleh kosong!";		
	}
	
	# JIKA ADA PESAN ERROR DARI VALIDASI
	if (count($pesanError)>=1 ){
		echo "<div class='mssgBox'>";
			$noPesan=0;
			foreach ($pesanError as $indeks=>$pesan_tampil) { 
			$noPesan++;
				echo "&nbsp;&nbsp; $noPesan. $pesan_tampil<br>";	
			} 
		echo "</div> <br>"; 
	}
	else{
# SIMPAN DATA KE DATABASE
# Jika jumlah error pesanError tidak ada, maka penyimpanan dilakukan. Data dari tmp dipindah ke tabel penjualan dan penjualan_item
		$mySql	= "INSERT INTO tmp_surat SET id='$id', no_surat='$no_surat', tingkat_biaya_p='$tingkat_biaya_p',
						tujuan='$tujuan', alat_angkut='$alat_angkut', tempat_berangkat='$tempat_berangkat',
						tempat_tujuan='$tempat_tujuan', lama_jalan='$lama_jalan',
						tgl_berangkat='".InggrisTgl($txtTanggal)."', 
						tgl_kembali='".InggrisTgl($txtTanggal)."',
						instansi='$instansi', akun='$akun', lain='$lain'";
		$myQry	= mysql_query($mySql, $koneksidb) or die ("Gagal query".mysql_error());	
		if($myQry){
			echo "<script>alert('Data pegawai Berhasil di simpan')</script>";
			echo "<meta http-equiv='refresh' content='0; url=?page=ddsurat'>";
		}
		exit;
		
	}					
		# Kosongkan Tmp jika datanya sudah dipindah	
		// $hapusSql = "DELETE FROM tmp_surat";
		// mysql_query($hapusSql, $koneksidb) or die ("Gagal kosongkan tmp".mysql_error());
		
		// Refresh form
		// echo "<script>";
		// echo "window.open('bayar_nota.php?noNota=$kd_pembayaran', width=12,height=12,left=12, top=25)";
		// echo "</script>";	
}
//==================================================================================================
//	=================================================================
if ((isset($_POST['submit1'])) AND ($_POST['nip'] <> "da"))
  {
  $nip1 = $_POST['nip'];
  $sql1 = mysql_query("SELECT * FROM pegawai WHERE nama_pegawai LIKE '%$nip1%' or nip LIKE '%$nip1%'") or die(mysql_error());
  $myData = mysql_fetch_array($sql1); 
  $dataNip= $myData['nip'];
  $dataNama		= $myData['nama_pegawai'];
  $dataPangkat		= $myData['pangkat'];
  $dataGolongan		= $myData['golongan'];
  $dataJabatan		= $myData['jabatan'];
  }
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>Autocomplete dari database dengan jQuery dan PHP  </title>
    <link rel="stylesheet" href="js/jquery-ui.css" />
    <script src="js/jquery-1.8.3.js"></script>
    <script src="js/jquery-ui.js"></script> 
    <script>
	/*autocomplete muncul setelah user mengetikan minimal2 karakter */
    $(function() {  
        $( "#anime" ).autocomplete({
         source: "data.php",  
           minLength:2, 
        });
    });
    </script>

<link rel="stylesheet" type="text/css" href="plugins/tigra_calendar/tcal.css"/>
<script type="text/javascript" src="plugins/tigra_calendar/tcal.js"></script> 
</head>

<body>   
<!-- <form action="?page=simpan_surat" method="post" name="form1" target="_self">    -->
<form action="<?php $_SERVER['PHP_SELF']; ?>" method="post">   
  <table align="left" class="table-striped table-bordered table-condensed">
    <tr>
      <td colspan="2" bgcolor="#CCCCCC"><strong>DATA PERJALANAN </strong></td>
      <td width="340">&nbsp;</td>
    </tr>
     <tr>
      <td width="210"><strong>Nomor Surat </strong></td>
      <td width="10"><strong>:</strong></td>
      <td width="340"><b>
        <input name="no_surat" value="<?php echo $dataNo; ?>" type="text"  size="20" maxlength="25"  />
      </b></td>
    </tr>
    <tr>
      <td colspan="2" bgcolor="#CCCCCC"><strong>INPUT PERJALANAN</strong></td>
      <td width="340">&nbsp;</td>
    </tr>
    <tr>
      <td width="210"><strong> Tingkat Biaya Perjalanan </strong></td>
      <td width="10"><strong>:</strong></td>
      <td width="340"><b>
        <input name="tingkat_biaya_p" type="text"  size="20" maxlength="25"  />
      </b></td>
    </tr>
    <tr>
      <td width="210"><strong>Maksud Perjalanan Dinas </strong></td>
      <td width="10"><strong>:</strong></td>
      <td width="340"><b>
        <!-- <input name="tujuan" type="text"  size="20" maxlength="25"  /> -->
        <textarea name='tujuan' type='text'></textarea>
      </b></td>
    </tr>
    <tr>
      <td width="210"><strong>Alat Angkut yang dipergunakan </strong></td>
      <td width="10"><strong>:</strong></td>
      <td width="340"><b>
        <input name="alat_angkut" type="text"  size="20" maxlength="25"  />
      </b></td>
    </tr>
    <tr>
      <td width="210"><strong>Tempat Berangkat </strong></td>
      <td width="10"><strong>:</strong></td>
      <td width="340"><b>
        <input name="tempat_berangkat" type="text"  size="20" maxlength="25"  />
      </b></td>
    </tr>
    <tr>
      <td width="210"><strong>Tempat Tujuan</strong></td>
      <td width="10"><strong>:</strong></td>
      <td width="340"><b>
        <input name="tempat_tujuan" type="text"  size="20" maxlength="25"  />
      </b></td>
    </tr>
<!-- ==================================================================================================== -->

    <tr>
      <td><strong>Tanggal Berangkat</strong></td>
      <td><strong>:</strong></td><!-- class="tcal" -->
      <td><input name="tgl_berangkat" id='tgl_berangkat' type="text" class='tcal' value="<?php echo $tgl_berangkat; ?>" size="20" maxlength="20" /></td>
    </tr>
    <tr>
      <td><strong>Tanggal Kembali</strong></td>
      <td><strong>:</strong></td>
      <td><input name="tgl_kembali" id='tgl_kembali' type="text" class='tcal' value="<?php echo $tgl_kembali; ?>" size="20" maxlength="20" /></td>
    </tr>

    <tr>
      <td width="210"><strong>Lamanya Perjalanan Dinas </strong></td>
      <td width="10"><strong>:</strong></td>
      <td width="340"><b>
        <input name="lama_jalan" id='lama_jalan' type="text"  size="20" maxlength="25"  />
      </b></td>
    </tr>

<!-- ================================================================================================== -->

	<tr>
      <td colspan="3"><strong>Pembeban Anggaran</strong></td>
    </tr>
    <tr>
      <td width="210"><strong>Instansi : </strong></td>
      <td width="10"><strong>:</strong></td>
      <td width="340"><b>
        <input name="instansi" type="text"  size="30" maxlength="50"  />
      </b></td>
    </tr>
    <tr>
      <td width="210"><strong>Akun : </strong></td>
      <td width="10"><strong>:</strong></td>
      <td width="340"><b>
        <input name="akun" type="text"  size="25" maxlength="25"  />
      </b></td>
    </tr>

    <tr>
      <td width="210"><strong>Keterangan Lain-lain : </strong></td>
      <td width="10"><strong>:</strong></td>
      <td width="340"><b>
        <textarea name="lain" type="text"></textarea>
        <!-- <input name="lain" type="text"  size="20" maxlength="25"  /> -->
      </b></td>
    </tr>

 	<tr>
      <td colspan="3" bgcolor="#CCCCCC" align="center"><input name="btnSimpan" type="submit"  class="btn btn-warning" style="cursor:pointer;" onclick="return confirm('ANDA YAKIN AKAN MENYIMPAN DATA  INI ... ?')" value="SIMPAN " /></td>
    </tr>
    </table>

<table align="right" class="table-striped table-bordered table-condensed">
  <tr>
      <td colspan="2" bgcolor="#CCCCCC"><strong>INPUT PEGAWAI</strong></td>
      <td width="356">&nbsp;</td>
  </tr>
  	<tr>
      <td width="210"><strong>NIP</strong></td>
      <td width="10"><strong>:</strong></td>
      <td width="356">
      <input placeholder="Cari data pegawai..." id="appendedInputButton" name="nip" value='<?php echo $dataNip ?>' type="text" size="20">
      <button class="btn-default" name="submit1" type="submit "><i class="fa fa-search"></i> Cari</button>
      </td>
    </tr>
    <tr>
      <td width="210"><strong>Nama Pegawai</strong></td>
      <td width="10"><strong>:</strong></td>
      <td width="356"><input name='nama_pegawai' value= "<?php echo $dataNama ?>" size="25" maxlength="30" /></td>
    </tr>
    <tr>
      <td width="210"><strong>Pangkat / Golongan</strong></td>
      <td width="10"><strong>:</strong></td>
      <td width="356"><input name='pangkat' value="<?php echo $dataPangkat; ?>" size="10" maxlength="40"  />/<input name='golongan' value="<?php echo $dataGolongan; ?>" size="3" maxlength="40"  /></td>
    </tr>
    <tr>
      <td width="210"><strong>Jabatan</strong></td>
      <td width="10"><strong>:</strong></td>
      <td width="356"><input name='jabatan' value="<?php echo $dataJabatan; ?>" size="40" maxlength="100" /></td>
    </tr>
    <tr>
      <td colspan="3" align="center"><b>
        <input name="btnTambah" class="btn btn-primary" type="submit" style="cursor:pointer;" onclick="return confirm('ANDA YAKIN AKAN MENAMBAHKAN DATA  INI ... ?')" value=" Tambah " />
      </b></td>
    </tr>
    </table>

<table align="right" class="table-striped table-bordered table-condensed">
    <tr>
      <th colspan="6">DAFTAR Pengikut</th>
    </tr>
    <tr>
      <td width="2" bgcolor="#CCCCCC"><strong>No</strong></td>
      <td width="100" bgcolor="#CCCCCC"><strong>NIP</strong></td>
      <td width="100" bgcolor="#CCCCCC"><strong>NAMA</strong></td>
      <td width="60" bgcolor="#CCCCCC"><strong>PANGKAT /GOLONGAN </strong></td>
      <td width="200" bgcolor="#CCCCCC"><strong>JABATAN</strong></td>
      <td width="20" bgcolor="#CCCCCC"><strong>Tool</strong></td>
    </tr>

<?php
// Qury menampilkan data dalam Grid 
$tmpSql ="SELECT  * FROM tmp_suratP  ORDER BY nip ";
$tmpQry = mysql_query($tmpSql, $koneksidb) or die ("Gagal Query Tmp".mysql_error());
$nomor=0;  
while($tmpData = mysql_fetch_array($tmpQry)) {
	$nomor++;
?>
    <tr>
      <td colspan="1" align="right"><?php echo $nomor; ?></td>
      <td width="2"><?php echo $tmpData['nip']; ?></b></td>
      <td width="20"><?php echo $tmpData['nama_pegawai']; ?></td>
      <td width="100"><?php echo $tmpData['pangkat']; ?>/<?php echo $tmpData['golongan']; ?></td>
      <td width="10"><?php echo $tmpData['jabatan']; ?></td>
      <td width="30"><a href="?page=hapus_tmpsurat&id=<?php echo $tmpData['id'];?>" target="_self" onClick="return confirm('Apakah anda yakin ingin menghapus data ini ?')">Hapus</a></td>
    </tr>
<?php } ?>
</table>
</form>
</body>
</html>